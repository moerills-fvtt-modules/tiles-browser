class TileBrowserApp extends FilePicker {
    constructor (options = {}) {
        super(options);

        this.thumbPath = "/thumbs";
        // this.type = "image";        
        
        this._initHooks();
        this._drop = ev => this._canvasDrop(ev);
        this._move = ev => this._canvasMouseMove(ev);
        this._wheel = ev => this._canvasWheel(ev);
        this._key = ev => this._canvasKey(ev);

        this.progress = null;

        this.thumbDir = "modules/tiles-browser/thumbs";

        this._el = null;
        this.prev = null;

        this._files = [];
        this._wildcard = false;
        this._refreshCache = false;
    }

    _initHooks() {
        Hooks.on("ready", () => {
            this.canvas = document.getElementById('board');
        });

        Hooks.on('getSceneControlButtons', controls => {
            let tiles = controls.find(e => e.name === "tiles");
            tiles.tools.push( {
                name: "Browse Tiles",
                icon: "fas fa-folder-open",
                onClick: () => {
                    this.render(true);
                    // activate select tool after opening the tiles browser
                    ui.controls.controls.find(e => e.name === "tiles").activeTool = "select";
                    ui.controls.render();
                }
            });

            if (game.user.isGM)
                tiles.tools.push( {
                    name: "render",
                    title: "Render Tiles to image",
                    icon: "fa fa-camera",
                    onClick: () => {
                        new RenderBrowser().render(true);
                        // this._renderTilesToImage();
                        // activate select tool after opening the tiles browser
                        ui.controls.controls.find(e => e.name === "tiles").activeTool = "select";
                        ui.controls.render();
                    }
                });
        });
    }

    static get defaultOptions() {
        let options = super.defaultOptions;
        options.template = "modules/tiles-browser/templates/tiles-browser.html";
        options.classes = ["tiles-browser"];
        options.width = 622;
        options.title = "Tile Browser";
        options.resizable = true;
        return options;
    }

    get title() {
        return "Tile Browser";
    }

    get canUpload() {
        return false;
    }

    async getData() {
        let data = await super.getData();
        data.dirs = data.dirs.filter(e => !e.name.includes("thumbs"))

        data.thumbsDir = this.thumbDir;
        // Map files to their name, extension and get their hash.
        this._files = data.files;
        data.wildcard = this._wildcard;
        if (this._refreshCache) {
            data.refreshStamp = Date.now();
        }
        data.files = data.files.map(e => {
            const tmp = e.name.split('.');
            return {
                extension: tmp.pop(),
                name: tmp.join('.').replace(/%20/g, ' '),
                hash: this._getStringHash(e.url),
                url: e.url
                };
        });
        return data;
    }

    _createProgBar() {
        if (this.progress !== null)
            return;
        const progDiv = this.element.find('.progress-div');
        progDiv.prepend(`
        <p><b>Creating missing thumbnails for this folder.</b></p>
        <p>Depending on your connection to the server this may take a while...</p>
        <div id='prog-bar'><div id='progress'></div></div>`)
        this.progress = progDiv.find('#progress');
    }

    async _createThumb(path) {
        const split = path.split("/");

        // let name = split.pop().replace(/\%20/g, " ");
        let texture = await loadTexture(path);
        // path = split.join("/");
        // Create the image
        let img = new PIXI.Sprite(texture);
        const ratio = texture.width / texture.height;
        if (ratio > 1) {
            img.width = 150;
            img.height = 150 / ratio;
        } else {
            img.width = 150 * ratio;
            img.height = 150;
        }
        // Mask the center to crop it to a RenderTexture
        let tex = PIXI.RenderTexture.create(img.width, img.height, PIXI.SCALE_MODES.LINEAR, 2);
        canvas.app.renderer.render(img, tex);
    
        // Export the rendered texture to base64
        let thumb = new PIXI.Sprite(tex);
        // save all as .webp to avoid problems with svgs
        // name = name.split('.').slice(0,-1).join('.') + ".webp"
        canvas.app.renderer.extract.canvas(thumb).toBlob(blob => this._uploadThumb(blob, path), 'image/webp', 0.7);
        tex.destroy(true);
    }

    _uploadThumb(blob, path) {
        let data = new FormData();
        data.append("target", this.thumbDir);
        data.append("upload", blob, this._getStringHash(path)+'.webp'); //path.split("/").pop()+"_"+name)
        data.append('source', 'data');

         // Create a POST request
        let xhr = new XMLHttpRequest();
        xhr.open('POST', '/upload', true);
        let upload = {};
        xhr.onloadstart = event => upload.disabled = true;
        xhr.onreadystatechange = () => {
            if ( xhr.readyState !== 4 ) return;

            this.thumbsFinished++;
            if (this.progress !== undefined) {
                const perc = Math.floor(parseFloat(this.thumbsFinished)/parseFloat(this.thumbsMissing)*100);
                this.progress.width(perc+("%"));    
            }
            if (this.thumbsFinished === this.thumbsMissing) {
                ui.notifications.info("Finished creating missing thumbnails!");
                this.render(true);
                this.progress = null;
            }

            if ( xhr.status === 500 ) ui.notifications.error(xhr.response);
            upload.disabled = false;
        };
        // Submit the POST request
        xhr.send(data);
    }

    _onMissingThumb(path) {
        // const path = 
        // Dont create thumbnails for thumb folders
        if (path.includes('thumbs'))
            return;
        this._createProgBar();
        this.thumbsMissing++;
        this._createThumb(path);
    }

    activateListeners(html) {
        super.activateListeners(html);
        this._refreshCache = false;
        this.thumbsMissing = 0;
        this.thumbsFinished = 0;
        let tiles = html.find('.tile');
        tiles.on('mousedown', ev => this._onMouseDown(ev));
        tiles.find('img').on('error', ev => this._onMissingThumb(ev.target.parentNode.getAttribute('data-path')))
        html.on('mouseup', ev => this._onMouseUp(ev));

        html.find('.refresh').on('click', ev => {
            this.thumbsMissing = 0;
            this.thumbsFinished = 0;
            this._refreshCache = true;
            html.find('.tile:visible').each((idx, e) => {
                this._onMissingThumb(e.getAttribute('data-path'))
            })
        })

        let scaleInp = html.find('.scale');
        scaleInp.on('input', ev => this.scale = ev.target.value);
        if (!isNaN(this.scale))
            scaleInp[0].value = this.scale;
        html.find('#wildcard').on('change', e => this._wildcard = e.target.checked)

    }

    _initDragListener() {
        this.canvas.addEventListener('mousemove', this._move);
        this.canvas.addEventListener('mouseup', this._drop);
        this.canvas.addEventListener('wheel', this._wheel);
        window.addEventListener('keyup', this._key)
    }

    _onMouseDown(ev) {
        if (ev.button === 0) {
            ev.preventDefault();
            ev.stopPropagation();
            canvas.activeLayer.releaseAll()
            this._el = ev.currentTarget;
            this._el.classList.add('selected')
            this._dragStart(ev.currentTarget.getAttribute('data-path'));
            this._initDragListener();
        } else if (ev.button < 3) {
            ev.preventDefault();
            ev.stopPropagation();
            this._dragEnd();
        } 
    }

    _onFilterResults(ev) {
        let input = ev.currentTarget;
        // Define filtering function
        let filter = query => {
          this.element.find('.tile-directory').children().each((i, li) => {
            li.hidden = !query.test(li.getAttribute("data-path"));
          });
        };
    
        // Filter if we are done entering keys
        let query = new RegExp(RegExp.escape(input.value), "i");
        filter(query);
      }
    

    _dragStart(path) {
        if (this._wildcard) {
            const els = this.element.find('.tile:visible');
            path = els[Math.floor(Math.random()*els.length)].dataset.path;
        }

        this._createPrev(path);
    }

    /**
     * Move preview with mouse
     * @param {*} ev 
     */
    _canvasMouseMove(ev) {
        if (this.prev === null) {
            this._dragEnd();
            return;
        }

        let mouse = {
            x: ev.clientX,
            y: ev.clientY
        }
        const t = canvas.activeLayer.worldTransform,
              tx = (mouse.x - t.tx) / canvas.stage.scale.x,
              ty = (mouse.y - t.ty) / canvas.stage.scale.y,
              offX = this.prev.data.width / 2,
              offY = this.prev.data.height / 2;

        let x = tx - offX,
            y = ty - offY;
        // If no modifier is pressed, snap to 1/4th grid
        if (!ev.shiftKey && !ev.altKey && !ev.ctrlKey) {
            // x = x + 2*offX;
            // y = y + 2*offY;
            const pos = canvas.grid.getSnappedPosition(x,y, 4);

            x = pos.x; // - 2*offX;
            y = pos.y; // - 2*offY;
        }
        this.prev.data.x = x;
        this.prev.data.y = y;

        this.prev.refresh();
    }

    async _createPrev(path) {
        console.log(path);  
        let width = 0,
            height = 0,
            name = path.split("/").pop().replace(/%20/g, ' '),
            dim = name.match(/\d+\s*x\s*\d+/),
            scale = this.scale || 1;
        // Try to get dimension in grid units from filename
        // Format: "3x3"
        if (dim !== null) {
            dim = dim[0].split("x");
            const x = Number(dim[0]),
                y = Number(dim[1]);
            width = canvas.grid.size * x;
            height = canvas.grid.size * y;
        } else {
            // Else get width/height from PIXI image texture
            const texture = await loadTexture(path);
            // Create the image
            const img = new PIXI.Sprite(texture);
            width = img.width;
            height = img.height;
            img.destroy();
        }
        let tile = new Tile({
            img: path,
            width: Math.round(width * scale),
            height: Math.round(height * scale),
            hidden: false,
            x: 0,
            y: 0,
            rotation: 0
        })
        await tile.draw();
        tile._controlled = true;
        // canvas.tiles.activate();
        this.prev = canvas.tiles.preview.addChild(tile);
    }


    _onMouseUp(ev) {
        ev.preventDefault();
        // Drag end on mouse left up or right up
        if (ev.button < 3)
            this._dragEnd();
    }

    _dragEnd() {
        if (this.prev !== null) {
            canvas.tiles.preview.removeChildren();
            this.prev = null;
        }
        if (this._el !== null) {
            this._el.classList.remove('selected');
            this._el = null;
        }
        this.canvas.removeEventListener('mousemove', this._move);
        this.canvas.removeEventListener('mouseup', this._drop);
        this.canvas.removeEventListener('wheel', this._wheel);
        window.removeEventListener('keyup', this._key);
    }

    _canvasDrop(ev) {
        if (this.prev === null || ev.button === 2) {
            this._dragEnd();
            return;
        }
        const data = duplicate(this.prev.data);
        this._dragEnd();
        canvas.scene.createEmbeddedEntity('Tile', data);
    }

    _canvasWheel(ev) {
        if (this.prev === null) {
            this._dragEnd();
            return;
        }

        if (ev.ctrlKey) {
            ev.preventDefault();
            ev.stopPropagation();
            let offset = (ev.deltaY < 0) ? 1 : -1;
            offset *= ev.shiftKey ? 15 : 1;
            this.prev.data.rotation += offset;
            this.prev.data.rotation = this.prev.data.rotation % 360;
            if (ev.shiftKey) { // snap to 15 degree angles
                const rot = this.prev.data.rotation;
                this.prev.data.rotation = Math.round(rot/15) * 15;
            }
        } else if (ev.altKey) {
            ev.preventDefault();
            ev.stopPropagation();
            let offset = ev.deltaY < 0 ? .05 : -0.05;
            offset *= ev.shiftKey ? 5 : 1;
            offset = 1 + offset;

            const minDim = canvas.grid.size / 5;
            this.prev.data.width = Math.max(this.prev.data.width * offset, minDim);
            this.prev.data.height = Math.max(this.prev.data.height * offset, minDim);
        } else {
            return false;
        }
        this._canvasMouseMove(ev);
    }

    _canvasKey(ev) {
        if (this.prev === null) {
            this._dragEnd();
            return;
        }

        if (ev.key === "y"  || ev.key === "Y" || ev.key === "Z" || ev.key === "z") {
            this.prev.data.y += this.prev.data.height;
            this.prev.data.height *= -1;
        } else if (ev.key === "x" || ev.key === "X") {
            this.prev.data.x += this.prev.data.width;
            this.prev.data.width *= -1;
        } else {
            return false;
        }
        ev.preventDefault();
        ev.stopPropagation();

        this.prev.refresh();
    }

    /** Taken from: https://werxltd.com/wp/2010/05/13/javascript-implementation-of-javas-string-hashcode-method/ */
    _getStringHash(str) {
        str = this.activeSource + "/" + str;
        var hash = 0, i, chr;
        if (str.length === 0) return hash;
        for (i = 0; i < str.length; i++) {
            chr = str.charCodeAt(i);
            hash =  ((hash << 5) - hash) + chr;
            hash |= 0; // Convert to 32bit Integer
        }
        return hash;
    }
}

Hooks.on('init', () => {
    new TileBrowserApp({type:"image"});
});