class RenderBrowser extends FilePicker {
	constructor(options={type: []}) {
		super(options);

		// not able to upload to FVTT directory
		delete this.sources.public;

		this.layers = {
			background: false,
			tiles: true,
			drawings: false,
			tokens: false
		}
	}

	async getData() {
		let data = await super.getData();
		data.name = canvas.scene.data.name.replace(/\s/g, "_");
		data.layers = this.layers
		console.log(data);
		return data;
	}

	static get defaultOptions() {
		let options = super.defaultOptions;
		options.template = "modules/tiles-browser/templates/render-browser.html";
		options.classes = ["tiles-browser"];
		options.width = 340;
		options.resizable = true;
		return options;
	}

	get title() {
		return "Choose upload folder..";
	}

	get canUpload() {
		return false;
	}

	activateListeners(html) {
		super.activateListeners(html);
		html.find('.layer-selection input[type="checkbox"]').on('change', ev => {
			this.layers[ev.currentTarget.id] = ev.currentTarget.checked;
		});
	}

	_onSubmit(ev) {
		ev.preventDefault();
		const name = this.element.find('#file-name').val();
		const replace = this.element.find('#update')[0].checked;
		const path = this.sources[this.activeSource].target;
		const dims = canvas.dimensions;
		const tex = PIXI.RenderTexture.create(canvas.dimensions.width, canvas.dimensions.height);
		// const tex = PIXI.RenderTexture.create(dims.sceneWidth, dims.sceneHeight);

		let new_texture = true;
		for (let [key,val] of Object.entries(this.layers)) {
			if (val === false)
				continue;
			canvas.app.renderer.render(canvas[key], tex, new_texture);
			new_texture = false;
		}
		
		tex.frame = new PIXI.Rectangle(dims.paddingX, dims.paddingY, dims.sceneWidth, dims.sceneHeight);
		tex.updateUvs();
		const thumb = new PIXI.Sprite(tex);
		console.log(thumb, tex)
		canvas.app.renderer.extract.canvas(thumb).toBlob(blob => this._uploadImg(blob, path, name, replace), 'image/webp', 1);
		// tex.destroy(true);

		this.close();
	}

	_uploadImg(blob, path, name, replace = false) {
		let data = new FormData();
		data.append("target", path);
		data.append("upload", blob, name+".webp");
		data.append('source', this.activeSource);

		 // Create a POST request
		let xhr = new XMLHttpRequest();
		xhr.open('POST', '/upload', true);
		let upload = {};
		xhr.onloadstart = event => upload.disabled = true;
		xhr.onreadystatechange = () => {
				if ( xhr.readyState !== 4 ) return;
				if ( xhr.status === 500 ) ui.notifications.error(xhr.response);
				else {
					ui.notifications.info("Finished uploading!");
					if (replace === true) {
						if (path.length > 0)
							path += "/"
						this._updateScene(path+name+".webp");
					}
						
				}
				upload.disabled = false;
		};
		// Submit the POST request
		xhr.send(data);
	}

	_updateScene(img) {

		const dims = canvas.dimensions;
		let udata = {
			// width: canvas.dimensions.width,
			// height: canvas.dimensions.height,
			width: dims.sceneWidth,
			height: dims.sceneHeight,
			img: img
		};
		if (this.layers.tiles === true)
			udata.tiles = [];
		if (this.layers.drawings === true)
			udata.drawings = [];

		const gx2 = canvas.scene.data.grid * 2;

		let walls = duplicate(canvas.scene.data.walls);
		const paddingX = (Math.ceil(0.5*udata.width / gx2) * gx2) * 0.5,
					paddingY = (Math.ceil(0.5*udata.height / gx2) * gx2) * 0.5;
		for (let i = 0; i < walls.length; i++) {
			let wall = walls[i];
			wall.c[0] += paddingX;
			wall.c[1] += paddingY;
			wall.c[2] += paddingX;
			wall.c[3] += paddingY;
		}
		udata.walls = walls;

		canvas.scene.update(udata).then(() => ui.notifications.notify("Finished updating scene!"));
	}
}