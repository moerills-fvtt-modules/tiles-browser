# Tiles Browser
[![PayPal](https://img.shields.io/badge/Buy%20Me%20A%20Coffee-PayPal-blue?style=flat-square)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=FYZ294SP2JBGS&source=url)  

**Thanks for all support for the last 8 months!  
Since its main feature is implemented into core now, Tiles Browser has served its purpose for the time being.  
The update will only show a thank you screen until you deactive/uninstall this module.**